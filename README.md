This is an old demo using XNA to render inside a WPF window. I showcase a simple particle system. I plan to revive this project to experiment with different rendering technologies like Vulkan when it will be released. If you can't build here's a youtube link showing the project running:

https://www.youtube.com/watch?v=O_etJEhBbAI