﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuerillaDemo.Particles
{
    public enum ParticlePreset
    {
        Standard,
        Fireworks,
        Explosion
    }
}
