﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GuerillaDemo.Particles
{
    public class Particle
    {
        #region Fields

        private Vector2 position;
        private Vector2 lastPosition;
        private Vector2 forces;
        private Vector2 initialScale;
        private Vector2 scale;
        private double rotationSpeed;
        private double orientation;
        private double lifetime;
        private double inverseLifetime;
        private byte colorRed;
        private byte colorGreen;
        private byte colorBlue;
        private double opacity;
        private double spinSpeed;
        private double spinAngle;

        #endregion //Fields

        #region Properties

        /// <summary>
        /// Particle position
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Particle last position
        /// </summary>
        public Vector2 LastPosition
        {
            get { return lastPosition; }
            set { lastPosition = value; }
        }

        /// <summary>
        /// Applied forces on the particle
        /// </summary>
        public Vector2 Forces
        {
            get { return forces; }
            set { forces = value; }
        }

        /// <summary>
        /// Particle Scale
        /// </summary>
        public Vector2 Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        /// <summary>
        /// Particle Initial Scale before any scale modifications
        /// </summary>
        public Vector2 InitialScale
        {
            get { return initialScale; }
            set { initialScale = value; }
        }

        /// <summary>
        /// Particle rotation speed
        /// </summary>
        public double RotationSpeed
        {
            get { return rotationSpeed; }
            set { rotationSpeed = value; }
        }

        /// <summary>
        /// Particle Orientation angle in degrees
        /// </summary>
        public double Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }

        /// <summary>
        /// Particle current life
        /// </summary>
        public double Lifetime
        {
            get { return lifetime; }
            set { lifetime = value; }
        }

        /// <summary>
        /// Particle current life inversed
        /// </summary>
        public double InverseLifetime
        {
            get { return inverseLifetime; }
            set { inverseLifetime = value; }
        }

        /// <summary>
        /// Particle Red channel
        /// </summary>
        public byte ColorRed
        {
            get { return colorRed; }
            set
            {
                colorRed = value;
            }
        }

        /// <summary>
        /// Particle green channel
        /// </summary>
        public byte ColorGreen
        {
            get { return colorGreen; }
            set
            {
                colorGreen = value;
            }
        }

        /// <summary>
        /// Particle blue channel
        /// </summary>
        public byte ColorBlue
        {
            get { return colorBlue; }
            set
            {
                colorBlue = value;
            }
        }

        /// <summary>
        /// Particle Opacity
        /// </summary>
        public double Opacity
        {
            get { return opacity; }
            set
            {
                opacity = value;
            }
        }

        /// <summary>
        /// Particle Spinning speed
        /// </summary>
        public double SpinSpeed
        {
            get { return spinSpeed; }
            set
            {
                spinSpeed = value;
            }
        }

        /// <summary>
        /// Particle spinning angle
        /// </summary>
        public double SpinAngle
        {
            get { return spinAngle; }
            set
            {
                spinAngle = value;
            }
        }

        #endregion //Properties

        #region Methods

        public void Update(double elapsedTime)
        {
            this.LastPosition = position;
            this.Position += forces * (float)elapsedTime;
            this.Orientation += rotationSpeed * elapsedTime;
            this.Orientation += spinSpeed * elapsedTime;
            this.Lifetime -= elapsedTime;
        }

        #endregion //Methods
    }
}
