﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuerillaDemo.Events;
using System.ComponentModel;

namespace GuerillaDemo.Particles
{
    public delegate void ParticlePresetChangedEventHandler(object sender, PresetChangedEventArgs e);

    /// <summary>
    /// Represents a particle system. For the purpose of the demo the system is limited to one emitter per system
    /// </summary>
    public class ParticleSystem
    {
        #region Fields

        private EffectMode mode_;
        private Emitter emitter_;
        private bool followPointer_; //indicates if the emitter should follow the mouse pointer in the screen
        private ParticlePreset preset_; //selected effect preset
        
        #endregion //Fields

        #region Events

        public event ParticlePresetChangedEventHandler PresetChanged;

        #endregion

        #region Constructor

        public ParticleSystem()
        {
            emitter_ = new Emitter();
            mode_ = EffectMode.Infinite;
            followPointer_ = true;
        }

        #endregion //Constuctor

        #region Properties

        /// <summary>
        /// Effect mode for the system
        /// </summary>
        public EffectMode Mode
        {
            get { return mode_; }
            set{ mode_ = value; }
        }

        /// <summary>
        /// The particle emitter of the system
        /// </summary>
        public Emitter Emitter
        {
            get { return emitter_; }
            set { emitter_ = value; }
        }

        /// <summary>
        /// Indicates if the effect will follow mouse movement
        /// </summary>
        public bool FollowPointer
        {
            get { return followPointer_; }
            set { followPointer_ = value; }
        }

        /// <summary>
        /// Effect preset values
        /// </summary>
        public ParticlePreset Preset
        {
            get { return preset_; }
            set 
            {
                if (preset_ == value)
                    return;

                preset_ = value;

                if (PresetChanged != null)
                    PresetChanged(this, new PresetChangedEventArgs(value));
            }
        }

        #endregion //Properties
    }
}
