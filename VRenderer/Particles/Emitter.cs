﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GuerillaDemo.Helpers;
using System.ComponentModel;

namespace GuerillaDemo.Particles
{
    public class Emitter
    {
        #region Fields

        private EffectMode mode;
        private DiffusionType diffusion_;
        private double currentDuration_;
        private double duration_;
        private double durationInversed_;
        private bool frontToBack_;
        private int maxParticles_;
        private double frequency_;
        private double minScaleX_;
        private double minScaleY_;
        private double maxScaleX_;
        private double maxScaleY_;
        private bool preserveRatio_;
        private double sprayAngle_;
        private int minQuantity_;
        private int maxQuantity_;
        private double minOpacity_;
        private double maxOpacity_;
        private double minSpeed_;
        private double maxSpeed_;
        private double minLife_;
        private double maxLife_;
        private byte startColorRed_;
        private byte startColorGreen_;
        private byte startColorBlue_;
        private byte startColorAlpha_;
        private byte endColorRed_;
        private byte endColorGreen_;
        private byte endColorBlue_;
        private byte endColorAlpha_;
        private double minRotationSpeed;
        private double maxRotationSpeed;
        private double minSpinAngle;
        private double maxSpinAngle;
        private double scaleModStart_;
        private double scaleModEnd_;
        private double minSpinSpeed_;
        private double maxSpinSpeed_;
        private double radius_;
        private double orientation_;
        private double diffusionX_;
        private double diffusionY_;
        private double opacityModifierStart_;
        private double opacityModifierEnd_;
        private int quantityModifierStart_;
        private int quantityModifierEnd_;
        private double positionX_;
        private double positionY_;
        private double emitTime_;
        private List<Particle> particles_;
        private Vector2 mouseOffset_;

        #endregion //Fields

        #region Properties

        /// <summary>
        /// Diffusion type when the particles are created
        /// </summary>
        public DiffusionType Diffusion
        {
            get { return diffusion_; }
            set
            {
                diffusion_ = value;
            }
        }

        /// <summary>
        /// Remaining duration to emit
        /// </summary>
        public double CurrentDuration
        {
            get { return currentDuration_; }
        }

        /// <summary>
        /// Duration of the effect
        /// </summary>
        public double Duration
        {
            get { return duration_; }
            set
            {
                currentDuration_ = value;
                duration_ = value;
                if (value != 0)
                    DurationInversed = 1 / value;
                
            }
        }

        /// <summary>
        /// Inversed duration
        /// </summary>
        public double DurationInversed
        {
            get { return durationInversed_; }
            set
            {
                durationInversed_ = value;
            }
        }

        /// <summary>
        /// Inserts new particles in the front of the list
        /// </summary>
        public bool FrontToBack
        {
            get { return frontToBack_; }
            set
            {
                frontToBack_ = value;
            }
        }

        /// <summary>
        /// Max number of available particles
        /// </summary>
        public int MaxParticles
        {
            get { return maxParticles_; }
            set
            {
                maxParticles_ = value;
            }
        }
        
        /// <summary>
        /// Frequency to emit particles in seconds
        /// </summary>
        public double Frequency
        {
            get { return frequency_; }
            set
            {
                frequency_ = value;
            }
        }

        /// <summary>
        /// Minimum x scale when initializing the particle
        /// </summary>
        public double MinScaleX
        {
            get { return minScaleX_; }
            set
            {
                minScaleX_ = value;
            }
        }

        /// <summary>
        /// Minimum y scale when initializing the particle
        /// </summary>
        public double MinScaleY
        {
            get { return minScaleY_; }
            set
            {
                minScaleY_ = value;
            }
        }

        /// <summary>
        /// Maximum x scale when initializing the particle
        /// </summary>
        public double MaxScaleX 
        { 
            get { return maxScaleX_; } 
            set 
            { 
                maxScaleX_ = value;  
            } 
        }
        
        /// <summary>
        /// Maximum y scale when initializing the particle
        /// </summary>
        public double MaxScaleY 
        { 
            get { return maxScaleY_; } 
            set 
            { 
                maxScaleY_ = value; 
            } 
        }

        /// <summary>
        /// Indicates if the scale on y will be the same as the scale on x when initializing
        /// </summary>
        public bool PreserveRatio
        {
            get { return preserveRatio_; }
            set
            {
                preserveRatio_ = value;
            }
        }

        /// <summary>
        /// Angle that the particles are emitted around the emitter
        /// </summary>
        public double SprayAngle
        {
            get { return sprayAngle_; }
            set
            {
                sprayAngle_ = value;
            }
        }

        /// <summary>
        /// Minimum quantity of particles when initializing
        /// </summary>
        public int MinQuantity
        {
            get { return minQuantity_; }
            set
            {
                minQuantity_ = value;
            }
        }
   
        /// <summary>
        /// Maximum quantity of particles when initializing
        /// </summary>
        public int MaxQuantity
        {
            get { return maxQuantity_; }
            set
            {
                maxQuantity_ = value;
            }
        }
       
        /// <summary> 
        /// Minimum particle opacity value when initializing
        /// </summary>
        public double MinOpacity
        {
            get { return minOpacity_; }
            set
            {
                minOpacity_ = value;
            }
        }
      
        /// <summary>
        /// Maximum particle opacity value when initializing
        /// </summary>
        public double MaxOpacity
        {
            get { return maxOpacity_; }
            set
            {
                maxOpacity_ = value;
            }
        }
        
        /// <summary>
        /// Minimum particle speed when initializing
        /// </summary>
        public double MinSpeed
        {
            get { return minSpeed_; }
            set
            {
                minSpeed_ = value;
            }
        }
        
        /// <summary>
        /// Maximum particle speed when initializing
        /// </summary>
        public double MaxSpeed
        {
            get { return maxSpeed_; }
            set
            {
                maxSpeed_ = value;
            }
        }
        
        /// <summary>
        /// Minimum particle life when initializing
        /// </summary>
        public double MinLife
        {
            get { return minLife_; }
            set
            {
                minLife_ = value;
            }
        }
        
        /// <summary>
        /// Maximum particle life when initializing
        /// </summary>
        public double MaxLife
        {
            get { return maxLife_; }
            set
            {
                maxLife_ = value;
            }
        }
        
        /// <summary>
        /// Starting red channel value for particle
        /// </summary>
        public byte ColorModStartRed
        {
            get { return startColorRed_; }
            set
            {
                startColorRed_ = value;
            }
        }
        
        /// <summary>
        /// Starting green channel value for particle
        /// </summary>
        public byte ColorModStartGreen
        {
            get { return startColorGreen_; }
            set
            {
                startColorGreen_ = value;
            }
        }
        
        /// <summary>
        /// Starting blue channel color for particle
        /// </summary>
        public byte ColorModStartBlue
        {
            get { return startColorBlue_; }
            set
            {
                startColorBlue_ = value;
            }
        }
        
        /// <summary>
        /// Starting alpha channel color for particle
        /// </summary>
        public byte ColorModStartAlpha
        {
            get { return startColorAlpha_; }
            set
            {
                startColorAlpha_ = value;
            }
        }
        
        /// <summary>
        /// Ending red channel color for particle
        /// </summary>
        public byte ColorModeEndRed
        {
            get { return endColorRed_; }
            set
            {
                endColorRed_ = value;
            }
        }
        
        /// <summary>
        /// Ending Green channel color for particle
        /// </summary>
        public byte ColorModeEndGreen
        {
            get { return endColorGreen_; }
            set
            {
                endColorGreen_ = value;
            }
        }
       
        /// <summary>
        /// Ending blue channel color for particle
        /// </summary>
        public byte ColorModeEndBlue
        {
            get { return endColorBlue_; }
            set
            {
                endColorBlue_ = value;
            }
        }
        
        /// <summary>
        /// Ending alpha channel color for particle
        /// </summary>
        public byte ColorModEndAlpha
        {
            get { return endColorAlpha_; }
            set
            {
                endColorAlpha_ = value;
            }
        }

        /// <summary>
        /// Minimum rotation speed for particle when initializing
        /// </summary>
        public double MinRotationSpeed 
        {
            get { return minRotationSpeed; } 
            set 
            { 
                minRotationSpeed = value; 
            }
        }

        /// <summary>
        /// Max rotation speed for particle when initializing
        /// </summary>
        public double MaxRotationSpeed 
        { 
            get { return maxRotationSpeed; } 
            set
            {
                maxRotationSpeed = value; 
            }
        }

        /// <summary>
        /// Minimum spinning angle when initializing a particle
        /// </summary>
        public double MinSpinAngle
        {
            get { return minSpinAngle; }
            set
            {
                minSpinAngle = value;
            }
        }
        
        /// <summary>
        /// Maximum spinning angle when initializing a particle
        /// </summary>
        public double MaxSpinAngle
        {
            get { return maxSpinAngle; }
            set
            {
                maxSpinAngle = value;
            }
        }
        
        /// <summary>
        /// Scale modifier starting value
        /// </summary>
        public double ScaleModStart
        {
            get { return scaleModStart_; }
            set
            {
                scaleModStart_ = value;
            }
        }
        
        /// <summary>
        /// Scale modifier ending value
        /// </summary>
        public double ScaleModEnd
        {
            get { return scaleModEnd_; }
            set
            {
                scaleModEnd_ = value;
            }
        }
        
        /// <summary>
        /// Minimum spinning speed when initializing a particle
        /// </summary>
        public double MinSpinSpeed
        {
            get { return minSpinSpeed_; }
            set
            {
                minSpinSpeed_ = value;
            }
        }
        
        /// <summary>
        /// Maximum spinning speed when initializing a particle
        /// </summary>
        public double MaxSpinSpeed
        {
            get { return maxSpinSpeed_; }
            set
            {
                maxSpinSpeed_ = value;
            }
        }
        
        /// <summary>
        /// Radius to scatter the particles when they are created
        /// </summary>
        public double Radius
        {
            get { return radius_; }
            set
            {
                radius_ = value;
            }
        }

        /// <summary>
        /// Mode of the particle effect. Only Infinite and one shot works
        /// </summary>
        public EffectMode Mode
        {
            get { return mode; }
            set
            {
                mode = value;
            }
        }
       
        /// <summary>
        /// Angle to emit the particles
        /// </summary>
        public double Orientation
        {
            get { return orientation_; }
            set
            {
                orientation_ = value;
            }
        }

        /// <summary>
        /// X axis diffusion value
        /// </summary>
        public double DiffusionX
        {
            get { return diffusionX_; }
            set
            {
                diffusionX_ = value;
            }
        }
        
        /// <summary>
        /// Y axis diffusion value
        /// </summary>
        public double DiffusionY
        {
            get { return diffusionY_; }
            set
            {
                diffusionY_ = value;
            }
        }

        /// <summary>
        /// Opacity modifier starting value
        /// </summary>
        public double OpacityModStart
        {
            get { return opacityModifierStart_; }
            set
            {
                opacityModifierStart_ = value;
            }
        }

        /// <summary>
        /// Opacity modifier ending value
        /// </summary>
        public double OpacityModEnd
        {
            get { return opacityModifierEnd_; }
            set
            {
                opacityModifierEnd_ = value;
            }
        }

        /// <summary>
        /// Quantity modifier start value
        /// </summary>
        public int QuantityModStart
        {
            get { return quantityModifierStart_; }
            set
            {
                quantityModifierStart_ = value;
            }
        }
        
        /// <summary>
        /// Quantity modifier ending value
        /// </summary>
        public int QuantityModEnd
        {
            get { return quantityModifierEnd_; }
            set
            {
                quantityModifierEnd_ = value;
            }
        }
        
        /// <summary>
        /// Emitter position on X axis
        /// </summary>
        public double PositionX
        {
            get { return positionX_; }
            set
            {
                positionX_ = value;
            }
        }
       
        /// <summary>
        /// Emitter position on Y axis
        /// </summary>
        public double PositionY
        {
            get { return positionY_; }
            set
            {
                positionY_ = value;
            }
        }

        /// <summary>
        /// Time elapsed since last emit
        /// </summary>
        public double EmitTime
        {
            get { return emitTime_; }
        }

        /// <summary>
        /// List of particles
        /// </summary>
        public List<Particle> Particles
        {
            get { return particles_; }
            private set { }
        }

        /// <summary>
        /// Mouse position offset when following the mouse
        /// </summary>
        public Vector2 MouseOffset
        {
            get { return mouseOffset_; }
            set
            {
                mouseOffset_ = value;
            }
        }
                  
        #endregion

        #region Constructor

        public Emitter()
        {
            minOpacity_ = 0.1;
            maxOpacity_ = 1;
            minSpeed_ = 32;
            maxSpeed_ = 96;
            minQuantity_ = 100;
            maxQuantity_ = 500;
            diffusion_ = DiffusionType.Point;
            minScaleX_ = 1;
            minScaleY_ = 1;
            maxScaleX_ = 1;
            maxScaleY_ = 1;
            startColorAlpha_ = 255;
            startColorRed_ = 255;
            startColorGreen_ = 0;
            startColorBlue_ = 0;
            endColorAlpha_ = 255;
            endColorRed_ = 255;
            endColorGreen_ = 155;
            endColorBlue_ = 0;
            maxParticles_ = 10000;
            frequency_ = 0.0;
            sprayAngle_ = 60;
            minLife_ = 0.2;
            maxLife_ = 1.3;
            duration_ = 1;
            preserveRatio_ = true;
            this.emitTime_ = 1.0;
            this.particles_ = new List<Particle>();
            scaleModStart_ = 0.3;
            scaleModEnd_ = 3.0;
            Orientation = 90;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Initialize a particle with random values between the initialization ranges.
        /// </summary>
        /// <param name="particle">The particle to initialize</param>
        private void InitializeParticle(Particle particle)
        {
            particle.Lifetime = RandomGenerator.RandomBetween(this.minLife_, this.MaxLife);
            particle.InverseLifetime = 1.0 / particle.Lifetime;
            particle.Opacity = RandomGenerator.RandomBetween(this.MinOpacity, this.MaxOpacity);
            particle.Orientation = RandomGenerator.RandomBetween(this.minSpinAngle, this.maxSpinAngle);
            particle.RotationSpeed = RandomGenerator.RandomBetween(this.MinRotationSpeed, this.MaxRotationSpeed);

            particle.ColorRed = startColorRed_;
            particle.ColorGreen = startColorGreen_;
            particle.ColorBlue = startColorBlue_;

            float scaleX = (float)RandomGenerator.RandomBetween(this.minScaleX_, this.maxScaleX_);
            float scaleY;

            if (!this.PreserveRatio)
                scaleY = (float)RandomGenerator.RandomBetween(this.minScaleY_, this.MaxScaleY);
            else
                scaleY = scaleX;
            particle.Scale = new Vector2(scaleX, scaleY);
            particle.InitialScale = particle.Scale;

            float directionAngle = (float)this.orientation_ + (float)RandomGenerator.RandomBetween(-this.sprayAngle_ * 0.5, sprayAngle_ * 0.5);
            float speed = (float)RandomGenerator.RandomBetween(this.MinSpeed, this.MaxSpeed);

            Matrix rotationMatrix = new Matrix();
            rotationMatrix = Matrix.Identity;
            Matrix.CreateRotationZ(-MathHelper.ToRadians(directionAngle), out rotationMatrix);
            Vector2 direction = new Vector2(speed, 0);
            direction = Vector2.Transform(direction, rotationMatrix);
            particle.Forces = new Vector2(direction.X, direction.Y);
        }

        /// <summary>
        /// Emits a number of particles
        /// </summary>
        /// <param name="count">The number of particles to emit</param>
        private void EmitParticles(int count)
        {
            //Only emit if there are available particles
            int freeParticles = this.maxParticles_ - particles_.Count;
            if (freeParticles <= 0)
                return;

            if (freeParticles < count)
                count = freeParticles;

            for (int i = 0; i < count; i++)
            {
                Particle particle = new Particle();

                //Calculate the initial position of the particle depending on the diffusion type
                if (this.diffusion_ == DiffusionType.Point)
                {
                    double randomAngle = RandomGenerator.RandomBetween(0, 360);
                    double randomRadius = RandomGenerator.RandomBetween(0, this.radius_);
                    double sin = Math.Sin(randomAngle);
                    double cos = Math.Cos(randomAngle);
                    float xPos = (float)(this.PositionX) + (float)(cos * randomRadius);
                    float yPos = (float)(this.PositionY) + (float)(sin * randomRadius);
                    particle.Position = new Vector2(xPos, yPos);
                }
                else if (this.diffusion_ == DiffusionType.LineX)
                {
                    float xPos = (float)((RandomGenerator.RandomBetween(0, this.diffusionX_) - this.diffusionX_ * 0.5));
                    particle.Position = new Vector2((float)(this.PositionX) + xPos, (float)(this.PositionY));
                }
                else if (this.diffusion_ == DiffusionType.LineY)
                {
                    float yPos = (float)(RandomGenerator.RandomBetween(0, this.diffusionY_) - this.diffusionY_ * 0.5);
                    particle.Position = new Vector2((float)(this.PositionX), (float)(this.PositionY) + yPos);
                }
                else if (this.diffusion_ == DiffusionType.Area)
                {
                    float xPos = (float)((RandomGenerator.RandomBetween(0, this.diffusionX_) - this.diffusionX_ * 0.5));
                    float yPos = (float)(RandomGenerator.RandomBetween(0, this.diffusionY_) - this.diffusionY_ * 0.5);
                    particle.Position = new Vector2((float)(this.PositionX) + xPos, (float)(this.PositionY) + yPos);
                }

                //Add the mouse offset in the position
                particle.Position += mouseOffset_;

                //Initialize
                this.InitializeParticle(particle);
                if (!this.frontToBack_)
                    particles_.Add(particle);
                else
                    particles_.Insert(0, particle);
            }
        }

        //Restard the emitter
        public void Restart()
        {
            currentDuration_ = duration_;
        }

        /// <summary>
        /// Update the particles 
        /// </summary>
        /// <param name="elapsedTime">Time elapsed since last update</param>
        public void Update(double elapsedTime)
        {
            //If there is remaining time for the effect
            if (currentDuration_ >= 0)
            {
                currentDuration_ -= elapsedTime;
                emitTime_ += elapsedTime;

                if (emitTime_ >= this.frequency_)
                {
                    int particlesCount = 0;
                    //Use quantity modifier
                    if (quantityModifierStart_ != quantityModifierEnd_)
                    {
                        float startQuantity = quantityModifierStart_;
                        float modifierFactor = (float)currentDuration_ * (float)durationInversed_;
                        float endQuantity = quantityModifierEnd_;
                        startQuantity *= modifierFactor;
                        endQuantity *= (1.0f - modifierFactor);
                        particlesCount = (int)(startQuantity + endQuantity);
                    }
                    else
                        particlesCount = (int)RandomGenerator.RandomBetween(this.minQuantity_, this.maxQuantity_);

                    EmitParticles(particlesCount);
                    emitTime_ = 0.0;
                }
            }

            for (int i = particles_.Count - 1; i >= 0; i--) //reverse iteration to remove dead particles on the fly
            {
                Particle particle = particles_[i];
                double modifierFactor = particle.Lifetime * particle.InverseLifetime;

                //if scale modifier start and end values are different modify particle scale
                if (scaleModStart_ != scaleModEnd_)
                {
                    double startScale = scaleModStart_;
                    double endScale = scaleModEnd_;
                    startScale *= modifierFactor;
                    endScale *= (1.0 - modifierFactor);
                    double scaleFactor = (startScale + endScale);
                    double scaleX = particle.InitialScale.X * scaleFactor;
                    double scaleY = particle.InitialScale.Y * scaleFactor;
                    particle.Scale = new Vector2((float)scaleX, (float)scaleY);
                }

                //If opacity modifier start and end values are different modify particle opacity
                if (opacityModifierStart_ != opacityModifierEnd_)
                {
                    double startOpacity = opacityModifierStart_;
                    double endOpacity = opacityModifierEnd_;
                    startOpacity *= modifierFactor;
                    endOpacity *= (1.0 - modifierFactor);
                    particle.Opacity = startOpacity + endOpacity;
                }

                //color variation by default
                double startRed = startColorRed_;
                double endRed = endColorRed_;
                startRed *= modifierFactor;
                endRed *= (1.0 - modifierFactor);
                particle.ColorRed = Convert.ToByte(startRed + endRed);

                double startGreen = startColorGreen_;
                double endGreen = endColorGreen_;
                startGreen *= modifierFactor;
                endGreen *= (1.0 - modifierFactor);
                particle.ColorGreen = Convert.ToByte(startGreen + endGreen);

                double startBlue = startColorBlue_;
                double endBlue = endColorBlue_;
                startBlue *= modifierFactor;
                endBlue *= (1.0 - modifierFactor);
                particle.ColorBlue = Convert.ToByte(startBlue + endBlue);

                //per particle rotation speed
                if (minSpinSpeed_ != maxSpinSpeed_)
                    particle.SpinSpeed = RandomGenerator.RandomBetween(this.minSpinSpeed_, this.maxSpinSpeed_);

                //per particle orientation
                if (minSpinAngle != maxSpinAngle)
                    particle.SpinAngle = RandomGenerator.RandomBetween(this.minSpinAngle, this.maxSpinAngle);

                particle.Update(elapsedTime);
                if (particle.Lifetime <= 0)
                    particles_.Remove(particles_[i]);  
            }
        }

        #endregion
    }
}
