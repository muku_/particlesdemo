﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuerillaDemo.Particles;

namespace GuerillaDemo.Events
{
    public class PresetChangedEventArgs : EventArgs
    {
        private ParticlePreset preset_;

        public ParticlePreset Preset
        {
            get { return preset_; }
            set { preset_ = value; }
        }

        public PresetChangedEventArgs(ParticlePreset preset)
        {
            preset_ = preset;
        }
    }
}
