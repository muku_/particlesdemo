﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using GuerillaDemo.Renderer;
using GuerillaDemo.ViewModel;
using GuerillaDemo.Particles;

namespace GuerillaDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer dispatcherTimer;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Create a particle system, initialize the viewmodels and add the particle system to the renderer
            ParticleSystem pSystem = new ParticleSystem();
            MainWindowViewModel vm = this.DataContext as MainWindowViewModel;
            vm.ShowParticles(pSystem);
            ParticlesRendererControl prControl = this.RendererHost.PRenderer;
            prControl.InitializeParticles(pSystem);

            //Create a timer to update the renderer
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Render)
            {
                Interval = TimeSpan.FromSeconds(0.010),
                IsEnabled = true
            };

            dispatcherTimer.Tick += delegate
            {
                double elapsedSeconds = prControl.StopWatch.Elapsed.TotalSeconds;
                double deltaTime = elapsedSeconds - prControl.LastTimerValue;
                prControl.LastTimerValue = elapsedSeconds;
                prControl.UpdateLogic(deltaTime);
                prControl.LastDeltaTime = deltaTime;
                prControl.Invalidate();
            };  
        }
    }
}
