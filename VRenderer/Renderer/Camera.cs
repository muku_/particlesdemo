﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GuerillaDemo.Renderer
{
    public class Camera
    {
        #region Fields

        private Vector2 zoom_;
        private Vector2 position_;
        private Vector2 pivot_;
        private Point viewPortPosition_;
        private Point viewPortSize_;
        private float rotation_;
        private Matrix matrix_;
        private bool computeMatrix_;

        #endregion

        #region Properties

        public Matrix Matrix
        {
            get { return matrix_; }
            set { matrix_ = value; }
        }

        public Vector2 Zoom
        {
            get { return zoom_; }
            set
            {
                if (zoom_ != value)
                {
                    zoom_ = value;
                    computeMatrix_ = true;
                }
            }
        }

        public Vector2 Pivot
        {
            get { return pivot_; }
            set
            {
                if (pivot_ != value)
                {
                    pivot_ = value;
                    computeMatrix_ = true;
                }
            }
        }

        public float Rotation
        {
            get { return rotation_; }
            set
            {
                if (rotation_ != value)
                {
                    rotation_ = value;
                    computeMatrix_ = true;
                }
            }
        }

        public Vector2 Position
        {
            get { return position_; }
            set
            {
                if (position_ != value)
                {
                    position_ = value;
                    computeMatrix_ = true;
                }
            }
        }

        public float PositionX
        {
            get { return position_.X; }
            set
            {
                if (position_.X != value)
                {
                    position_.X = value;
                    computeMatrix_ = true;
                }
            }
        }

        public float PositionY
        {
            get { return position_.Y; }
            set
            {
                if (position_.Y != value)
                {
                    position_.Y = value;
                    computeMatrix_ = true;
                }
            }
        }

        public Point ViewPortPosition
        {
            get { return viewPortPosition_; }
            set { viewPortPosition_ = value; }
        }

        public Point ViewPortSize
        {
            get
            {
                return viewPortSize_;
            }
            set
            {
                if (viewPortSize_ != value)
                {
                    viewPortSize_ = value;
                    computeMatrix_ = true;
                }
            }
        }

        #endregion

        #region Constructor

        public Camera(GraphicsDevice graphicsDevice)
        {
            position_ = Vector2.Zero;
            viewPortPosition_ = Point.Zero;
            zoom_ = Vector2.One;
            rotation_ = 0f;
            matrix_ = Matrix.CreateScale(1.0f, 1.0f, 1.0f);
            computeMatrix_ = true;
            this.Pivot = new Vector2(0.5f);
            if (graphicsDevice != null)
            {
                PresentationParameters pp = graphicsDevice.PresentationParameters;
                // full size by default
                viewPortSize_ = new Point(pp.BackBufferWidth, pp.BackBufferHeight);
            }
        }

        #endregion

        #region Methods

        public void Update()
        {
            if (computeMatrix_)
            {
                ComputeMatrix();
            }
        }

        private void ComputeMatrix()
        {
            Vector3 offset;
            offset = new Vector3(Pivot.X * this.ViewPortSize.X, Pivot.Y * this.ViewPortSize.Y, 0);
            matrix_ = Matrix.CreateTranslation(new Vector3(-this.Position.X, -this.Position.Y, 0)) *
                Matrix.CreateScale(Zoom.X, Zoom.Y, 1.0f) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateTranslation(offset);
            computeMatrix_ = false;
        }

        /// <summary>
        /// Translate an absolute screen position (i.e. mouse coord) into a world position, using this Camera
        /// </summary>
        public Vector2 ConvertToWorldPos(Vector2 position)
        {
            return Vector2.Transform(position, Matrix.Invert(matrix_));
        }

        /// <summary>
        /// Translate a world position into an absolute screen position, using this Camera
        /// </summary>
        public Vector2 ConvertToScreenPos(Vector2 position)
        {
            return Vector2.Transform(position, matrix_);
        }

        #endregion
    }
}
