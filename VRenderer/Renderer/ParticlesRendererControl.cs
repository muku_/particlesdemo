﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using GuerillaDemo.Particles;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using GuerillaDemo.Renderer.XNA;
using GuerillaDemo.Events;

namespace GuerillaDemo.Renderer
{
    public class ParticlesRendererControl : GraphicsDeviceControl
    {
        #region Fields

        ParticleSystem particleSystem_;
        SpriteBatch spriteBatch_;
        Color clearColor_;
        Texture2D particleTexture_;
        ContentManager contentManager_;
        Vector2 mousePosition_;
        BlendState blendState_;

        #endregion //Fields

        #region Properties

        /// <summary>
        /// The particle effect system to render in the screen
        /// </summary>
        public ParticleSystem ParticleSystem
        {
            get { return particleSystem_; }
            set { particleSystem_ = value; }
        }

        #endregion //Properties

        #region Methods


        protected override void Initialize()
        {
            spriteBatch_ = new SpriteBatch(this.GraphicsDevice);
            clearColor_ = new Color(8, 29, 46, 255);
            contentManager_ = new ResourceContentManager(this.Services, ContentResources.ResourceManager);
            particleTexture_ = contentManager_.Load<Texture2D>("circle");
            LastTimerValue = this.StopWatch.Elapsed.TotalSeconds;
            mousePosition_ = new Vector2();
            blendState_ = BlendState.AlphaBlend;
        }

        public void InitializeParticles(ParticleSystem pSystem)
        {
            particleSystem_ = pSystem;
            particleSystem_.PresetChanged += onParticlePresetChanged;
        }

        public override void UpdateLogic(double gameTime)
        {
            this.GraphicsDevice.Clear(clearColor_);
            Camera.Update();

            //If mode is Infinite restart when duration reaches 0
            if (particleSystem_.Mode == EffectMode.Infinite)
            {
                if (particleSystem_.Emitter.CurrentDuration <= 0)
                    particleSystem_.Emitter.Restart();
            }
            particleSystem_.Emitter.Update(gameTime);
        }

        protected override void Draw(double gameTime)
        {
            if (particleSystem_ == null)
                return;

            Emitter emitter = particleSystem_.Emitter;
            spriteBatch_.Begin(SpriteSortMode.Deferred, blendState_, SamplerState.LinearWrap, DepthStencilState.Default,
                               RasterizerState.CullNone, null, Camera.Matrix);
            for (int i = 0; i < emitter.Particles.Count; i++)
            {
               Color tint = new Color(emitter.Particles[i].ColorRed, emitter.Particles[i].ColorGreen, emitter.Particles[i].ColorBlue, (byte)(emitter.Particles[i].Opacity * 255f));
               this.spriteBatch_.Draw(particleTexture_, new Vector2((float)emitter.Particles[i].Position.X, (float)emitter.Particles[i].Position.Y), null,
                    tint, -MathHelper.ToRadians((float)emitter.Particles[i].Orientation), new Vector2(particleTexture_.Width / 2, particleTexture_.Height / 2),
                    new Vector2((float)emitter.Particles[i].Scale.X, (float)emitter.Particles[i].Scale.Y), SpriteEffects.None, 1);
            }
            spriteBatch_.End();
        }

        
        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            this.Focus();
            if (particleSystem_.FollowPointer)
            {
                mousePosition_ = Camera.ConvertToWorldPos(new Vector2(e.X, e.Y));
                particleSystem_.Emitter.MouseOffset = mousePosition_;
            }
        }

        /// <summary>
        /// Sets up the renderer when the preset changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onParticlePresetChanged(object sender, PresetChangedEventArgs e)
        {
            if (e.Preset == ParticlePreset.Explosion)
            {
                particleTexture_ = contentManager_.Load<Texture2D>("explosion");
                blendState_ = new BlendState
                {
                    AlphaSourceBlend = Blend.SourceAlpha,
                    ColorSourceBlend = Blend.SourceAlpha,
                    AlphaDestinationBlend = Blend.One,
                    ColorDestinationBlend = Blend.One,
                };
                //particleSystem_.Emitter.SetExplosionEffect();
            }
            else if (e.Preset == ParticlePreset.Standard)
            {
                particleTexture_ = contentManager_.Load<Texture2D>("circle");
                blendState_ = BlendState.AlphaBlend;
                //particleSystem_.Emitter.SetStandardEffect();
            }
            else
            {
                particleTexture_ = contentManager_.Load<Texture2D>("swirl");
                blendState_ = new BlendState
                {
                    AlphaSourceBlend = Blend.SourceAlpha,
                    ColorSourceBlend = Blend.SourceAlpha,
                    AlphaDestinationBlend = Blend.One,
                    ColorDestinationBlend = Blend.One,
                };
                //particleSystem_.Emitter.SetFireworksEffect();
            }
            
        }

        #endregion //Methods
    }
}