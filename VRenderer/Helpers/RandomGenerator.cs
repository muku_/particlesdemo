﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuerillaDemo.Helpers
{
    public static class RandomGenerator
    {
        public static readonly Random Random = new Random();

        public static double RandomBetween(double min, double max)
        {
            return min + Random.NextDouble() * (max - min);
        }
    }
}
