﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace GuerillaDemo.Helpers.Converters
{
    /// <summary>
    /// Comma Separated Value to Class Fields Converter.
    /// It will convert the csv from textboxes with X,Y or Min,Max and return the appropriate values to our classes and vice versa.
    /// We use only two values in each textbox. This converter is not limited to the amount of properties it can handle.
    /// </summary>
    public class CSVToFieldsConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            StringBuilder sb = new StringBuilder();

            foreach (object value in values)
            {
                if (value == null)
                    continue;

                if (sb.Length > 0)
                    sb.Append(",");

                sb.Append(value.ToString());
            }

            return sb.ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value == null)
                throw new NullReferenceException("value");

            if(parameter == null)
                throw new NullReferenceException("parameter");

            int size;
            if (!int.TryParse(parameter.ToString(), out size))
                throw new ArgumentException("parameter", "You need to pass the number of fields to convert to."); 

            string[] string_values = value.ToString().Split(',');
            if (string_values.Length != size)
                return null;

            object[] ret_values = new object[size];

            for(int i = 0; i <string_values.Length; i++)
            {
                ret_values[i] = System.Convert.ChangeType(string_values[i], targetTypes[0]);
            }

            return ret_values;
        }
    }
}