﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuerillaDemo.Renderer;
using System.Collections.ObjectModel;
using GuerillaDemo.Particles;

namespace GuerillaDemo.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Fields

        private ObservableCollection<WorkspaceViewModel> workspaces_;

        #endregion //fields

        #region Constructor

        public MainWindowViewModel()
        {
            workspaces_ = new ObservableCollection<WorkspaceViewModel>();
        }

        #endregion //Constructor

        #region Properties

        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get { return workspaces_; }
        }

        #endregion //Properties

        #region Methods

        /// <summary>
        /// Initializes ParticlesViewModel with the given particle system
        /// </summary>
        /// <param name="pSystem">The particle system to initialize the viewmodel with</param>
        public void ShowParticles(ParticleSystem pSystem)
        {   
            ParticlesViewModel vm = new ParticlesViewModel(pSystem);
            this.Workspaces.Add(vm);
        }

        #endregion //Methods        
    }
}
