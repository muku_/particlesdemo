﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuerillaDemo.Particles;

namespace GuerillaDemo.ViewModel
{
    public class ParticlesViewModel : WorkspaceViewModel
    {
        #region Fields

        private ParticleSystem particleSystem_;

        #endregion //Fields

        #region Constructor

        public ParticlesViewModel(ParticleSystem particleSystem)
        {
            particleSystem_ = particleSystem;
        }

        #endregion //Constructor

        #region Properties

        public EffectMode EffectMode
        {
            get { return particleSystem_.Mode; }
            set
            {
                if (particleSystem_.Mode == value)
                    return;

                particleSystem_.Mode = value;
                base.OnPropertyChanged("EffectMode");
            }
        }

        public DiffusionType DiffusionType
        {
            get { return particleSystem_.Emitter.Diffusion; }
            set
            {
                if (particleSystem_.Emitter.Diffusion == value)
                    return;

                particleSystem_.Emitter.Diffusion = value;
                base.OnPropertyChanged("DiffusionType");
            }
        }

        public int MaxParticles
        {
            get { return particleSystem_.Emitter.MaxParticles; }
            set
            {
                if (particleSystem_.Emitter.MaxParticles == value)
                    return;

                particleSystem_.Emitter.MaxParticles = value;
                base.OnPropertyChanged("MaxParticles");
            }
        }

        public double Frequency
        {
            get { return particleSystem_.Emitter.Frequency; }
            set
            {
                if (particleSystem_.Emitter.Frequency == value)
                    return;

                particleSystem_.Emitter.Frequency = value;
                base.OnPropertyChanged("Frequency");
            }
        }

        public double Angle
        {
            get { return particleSystem_.Emitter.SprayAngle; }
            set
            {
                if (particleSystem_.Emitter.SprayAngle == value)
                    return;

                particleSystem_.Emitter.SprayAngle = value;
                base.OnPropertyChanged("Angle");
            }
        }

        public double Orientation
        {
            get { return particleSystem_.Emitter.Orientation; }
            set
            {
                if (particleSystem_.Emitter.Orientation == value)
                    return;

                particleSystem_.Emitter.Orientation = value;
                base.OnPropertyChanged("Orientation");
            }
        }

        public double Radius
        {
            get { return particleSystem_.Emitter.Radius; }
            set
            {
                if (particleSystem_.Emitter.Radius == value)
                    return;

                particleSystem_.Emitter.Radius = value;
                base.OnPropertyChanged("Radius");
            }
        }

        public double MinSpeed
        {
            get { return particleSystem_.Emitter.MinSpeed; }
            set
            {
                if (particleSystem_.Emitter.MinSpeed == value)
                    return;

                particleSystem_.Emitter.MinSpeed = value;
                base.OnPropertyChanged("MinSpeed");
            }
        }

        public double MaxSpeed
        {
            get { return particleSystem_.Emitter.MaxSpeed; }
            set
            {
                if (particleSystem_.Emitter.MaxSpeed == value)
                    return;

                particleSystem_.Emitter.MaxSpeed = value;
                base.OnPropertyChanged("MaxSpeed");
            }
        }

        public double MinSpinSpeed
        {
            get { return particleSystem_.Emitter.MinSpinSpeed; }
            set
            {
                if (particleSystem_.Emitter.MinSpinSpeed == value)
                    return;

                particleSystem_.Emitter.MinSpinSpeed = value;
                base.OnPropertyChanged("MinSpinSpeed");
            }
        }

        public double MaxSpinSpeed
        {
            get { return particleSystem_.Emitter.MaxSpinSpeed; }
            set
            {
                if (particleSystem_.Emitter.MaxSpinSpeed == value)
                    return;

                particleSystem_.Emitter.MaxSpinSpeed = value;
                base.OnPropertyChanged("MaxSpinSpeed");
            }
        }

        public double MinSpinAngle
        {
            get { return particleSystem_.Emitter.MinSpinAngle; }
            set
            {
                if (particleSystem_.Emitter.MinSpinAngle == value)
                    return;

                particleSystem_.Emitter.MinSpinAngle = value;
                base.OnPropertyChanged("MinSpinAngle");
            }
        }

        public double MaxSpinAngle
        {
            get { return particleSystem_.Emitter.MaxSpinAngle; }
            set
            {
                if (particleSystem_.Emitter.MaxSpinAngle == value)
                    return;

                particleSystem_.Emitter.MaxSpinAngle = value;
                base.OnPropertyChanged("MaxSpinAngle");
            }
        }

        public double MinOpacity
        {
            get { return particleSystem_.Emitter.MinOpacity; }
            set
            {
                if (particleSystem_.Emitter.MinOpacity == value)
                    return;

                particleSystem_.Emitter.MinOpacity = value;
                base.OnPropertyChanged("MinOpacity");
            }
        }

        public double MaxOpacity
        {
            get { return particleSystem_.Emitter.MaxOpacity; }
            set
            {
                if (particleSystem_.Emitter.MaxOpacity == value)
                    return;

                particleSystem_.Emitter.MaxOpacity = value;
                base.OnPropertyChanged("MaxOpacity");
            }
        }

        public double MinLife
        {
            get { return particleSystem_.Emitter.MinLife; }
            set
            {
                if (particleSystem_.Emitter.MinLife == value)
                    return;

                particleSystem_.Emitter.MinLife = value;
                base.OnPropertyChanged("MinLife");
            }
        }

        public double MaxLife
        {
            get { return particleSystem_.Emitter.MaxLife; }
            set
            {
                if (particleSystem_.Emitter.MaxLife == value)
                    return;

                particleSystem_.Emitter.MaxLife = value;
                base.OnPropertyChanged("MaxLife");
            }
        }

        public double MinScaleX
        {
            get { return particleSystem_.Emitter.MinScaleX; }
            set
            {
                if (particleSystem_.Emitter.MinScaleX == value)
                    return;

                particleSystem_.Emitter.MinScaleX = value;
                base.OnPropertyChanged("MinScaleX");
            }
        }

        public double MaxScaleX
        {
            get { return particleSystem_.Emitter.MaxScaleX; }
            set
            {
                if (particleSystem_.Emitter.MaxScaleX == value)
                    return;

                particleSystem_.Emitter.MaxScaleX = value;
                base.OnPropertyChanged("MaxScaleX");
            }
        }

        public double MinScaleY
        {
            get { return particleSystem_.Emitter.MinScaleY; }
            set
            {
                if (particleSystem_.Emitter.MinScaleY == value)
                    return;

                particleSystem_.Emitter.MinScaleY = value;
                base.OnPropertyChanged("MinScaleY");
            }
        }

        public double MaxScaleY
        {
            get { return particleSystem_.Emitter.MaxScaleY; }
            set
            {
                if (particleSystem_.Emitter.MaxScaleY == value)
                    return;

                particleSystem_.Emitter.MaxScaleY = value;
                base.OnPropertyChanged("MaxScaleY");
            }
        }

        public int MinQuantity
        {
            get { return particleSystem_.Emitter.MinQuantity; }
            set
            {
                if (particleSystem_.Emitter.MinQuantity == value)
                    return;

                particleSystem_.Emitter.MinQuantity = value;
                base.OnPropertyChanged("MinQuantity");
            }
        }

        public int MaxQuantity
        {
            get { return particleSystem_.Emitter.MaxQuantity; }
            set
            {
                if (particleSystem_.Emitter.MaxQuantity == value)
                    return;

                particleSystem_.Emitter.MaxQuantity = value;
                base.OnPropertyChanged("MaxQuantity");
            }
        }

        public byte ColorModStartRed
        {
            get { return particleSystem_.Emitter.ColorModStartRed; }
            set
            {
                if (particleSystem_.Emitter.ColorModStartRed == value)
                    return;

                particleSystem_.Emitter.ColorModStartRed = value;
                base.OnPropertyChanged("ColorModStartRed");
            }
        }

        public byte ColorModStartGreen
        {
            get { return particleSystem_.Emitter.ColorModStartGreen; }
            set
            {
                if (particleSystem_.Emitter.ColorModStartGreen == value)
                    return;

                particleSystem_.Emitter.ColorModStartGreen = value;
                base.OnPropertyChanged("ColorModStartGreen");
            }
        }

        public byte ColorModStartBlue
        {
            get { return particleSystem_.Emitter.ColorModStartBlue; }
            set
            {
                if (particleSystem_.Emitter.ColorModStartBlue == value)
                    return;

                particleSystem_.Emitter.ColorModStartBlue = value;
                base.OnPropertyChanged("ColorModStartBlue");
            }
        }

        public byte ColorModStartAlpha
        {
            get { return particleSystem_.Emitter.ColorModStartAlpha; }
            set
            {
                if (particleSystem_.Emitter.ColorModStartAlpha == value)
                    return;

                particleSystem_.Emitter.ColorModStartAlpha = value;
                base.OnPropertyChanged("ColorModStartAlpha");
            }
        }

        public byte ColorModEndRed
        {
            get { return particleSystem_.Emitter.ColorModeEndRed; }
            set
            {
                if (particleSystem_.Emitter.ColorModeEndRed == value)
                    return;

                particleSystem_.Emitter.ColorModeEndRed = value;
                base.OnPropertyChanged("ColorModEndRed");
            }
        }

        public byte ColorModEndGreen
        {
            get { return particleSystem_.Emitter.ColorModeEndGreen; }
            set
            {
                if (particleSystem_.Emitter.ColorModeEndGreen == value)
                    return;

                particleSystem_.Emitter.ColorModeEndGreen = value;
                base.OnPropertyChanged("ColorModEndGreen");
            }
        }

        public byte ColorModEndBlue
        {
            get { return particleSystem_.Emitter.ColorModeEndBlue; }
            set
            {
                if (particleSystem_.Emitter.ColorModeEndBlue == value)
                    return;

                particleSystem_.Emitter.ColorModeEndBlue = value;
                base.OnPropertyChanged("ColorModEndBlue");
            }
        }

        public byte ColorModEndAlpha
        {
            get { return particleSystem_.Emitter.ColorModEndAlpha; }
            set
            {
                if (particleSystem_.Emitter.ColorModEndAlpha == value)
                    return;

                particleSystem_.Emitter.ColorModEndAlpha = value;
                base.OnPropertyChanged("ColorModEndAlpha");
            }
        }

        public double LineX
        {
            get { return particleSystem_.Emitter.DiffusionX; }
            set
            {
                if (particleSystem_.Emitter.DiffusionX == value)
                    return;

                particleSystem_.Emitter.DiffusionX = value;
                base.OnPropertyChanged("LineX");
            }
        }

        public double LineY
        {
            get { return particleSystem_.Emitter.DiffusionY; }
            set
            {
                if (particleSystem_.Emitter.DiffusionY == value)
                    return;

                particleSystem_.Emitter.DiffusionY = value;
                base.OnPropertyChanged("LineY");
            }
        }

        public double MinRotationSpeed
        {
            get { return particleSystem_.Emitter.MinRotationSpeed; }
            set
            {
                if (particleSystem_.Emitter.MinRotationSpeed == value)
                    return;

                particleSystem_.Emitter.MinRotationSpeed = value;
                base.OnPropertyChanged("MinRotationSpeed");
            }
        }

        public double MaxRotationSpeed
        {
            get { return particleSystem_.Emitter.MaxRotationSpeed; }
            set
            {
                if (particleSystem_.Emitter.MaxRotationSpeed == value)
                    return;

                particleSystem_.Emitter.MaxRotationSpeed = value;
                base.OnPropertyChanged("MaxRotationSpeed");
            }
        }

        public int QuantityModStart
        {
            get { return particleSystem_.Emitter.QuantityModStart; }
            set
            {
                if (particleSystem_.Emitter.QuantityModStart == value)
                    return;

                particleSystem_.Emitter.QuantityModStart = value;
                base.OnPropertyChanged("QuantityModStart");
            }
        }

        public int QuantityModEnd
        {
            get { return particleSystem_.Emitter.QuantityModEnd; }
            set
            {
                if (particleSystem_.Emitter.QuantityModEnd == value)
                    return;

                particleSystem_.Emitter.QuantityModEnd = value;
                base.OnPropertyChanged("QuantityModEnd");
            }
        }

        public double ScaleModStart
        {
            get { return particleSystem_.Emitter.ScaleModStart; }
            set
            {
                if (particleSystem_.Emitter.ScaleModStart == value)
                    return;

                particleSystem_.Emitter.ScaleModStart = value;
                base.OnPropertyChanged("ScaleModStart");
            }
        }

        public double ScaleModEnd
        {
            get { return particleSystem_.Emitter.ScaleModEnd; }
            set
            {
                if (particleSystem_.Emitter.ScaleModEnd == value)
                    return;

                particleSystem_.Emitter.ScaleModEnd = value;
                base.OnPropertyChanged("ScaleModEnd");
            }
        }

        public double OpacityModStart
        {
            get { return particleSystem_.Emitter.OpacityModStart; }
            set
            {
                if (particleSystem_.Emitter.OpacityModStart == value)
                    return;

                particleSystem_.Emitter.OpacityModStart = value;
                base.OnPropertyChanged("OpacityModStart");
            }
        }

        public double OpacityModEnd
        {
            get { return particleSystem_.Emitter.OpacityModEnd; }
            set
            {
                if (particleSystem_.Emitter.OpacityModEnd == value)
                    return;

                particleSystem_.Emitter.OpacityModEnd = value;
                base.OnPropertyChanged("OpacityModEnd");
            }
        }

        public bool PreserveRatio
        {
            get { return particleSystem_.Emitter.PreserveRatio; }
            set
            {
                if (particleSystem_.Emitter.PreserveRatio == value)
                    return;

                particleSystem_.Emitter.PreserveRatio = value;
                base.OnPropertyChanged("PreserveRatio");
            }
        }

        public bool FollowPointer
        {
            get { return particleSystem_.FollowPointer; }
            set
            {
                if (particleSystem_.FollowPointer == value)
                    return;

                particleSystem_.FollowPointer = value;

                if(value == false)
                    particleSystem_.Emitter.MouseOffset = new Microsoft.Xna.Framework.Vector2();

                base.OnPropertyChanged("FollowPointer");
            }
        }

        public ParticlePreset Preset
        {
            get { return particleSystem_.Preset; }
            set
            {
                if (particleSystem_.Preset == value)
                    return;

                particleSystem_.Preset = value;
                base.OnPropertyChanged("Preset");
                SetCurrentEffect(value);
            }
        }

        private void SetCurrentEffect(ParticlePreset preset)
        {
            if (preset == ParticlePreset.Standard)
            {
                particleSystem_.Emitter.MinOpacity = 0.1;
                base.OnPropertyChanged("MinOpacity");
                particleSystem_.Emitter.MaxOpacity = 1;
                base.OnPropertyChanged("MaxOpacity");
                particleSystem_.Emitter.MinSpeed = 32;
                base.OnPropertyChanged("MinSpeed");
                particleSystem_.Emitter.MaxSpeed = 96;
                base.OnPropertyChanged("MaxSpeed");
                particleSystem_.Emitter.MinQuantity = 100;
                base.OnPropertyChanged("MinQuantity");
                particleSystem_.Emitter.MaxQuantity = 500;
                base.OnPropertyChanged("MaxQuantity");
                particleSystem_.Emitter.Diffusion = DiffusionType.Point;
                base.OnPropertyChanged("DiffusionType");
                particleSystem_.Emitter.MinScaleX = 1;
                base.OnPropertyChanged("MinScaleX");
                particleSystem_.Emitter.MinScaleY = 1;
                base.OnPropertyChanged("MinScaleY");
                particleSystem_.Emitter.MaxScaleX = 1;
                base.OnPropertyChanged("MaxScaleX");
                particleSystem_.Emitter.MaxScaleY = 1;
                base.OnPropertyChanged("MaxScaleY");
                particleSystem_.Emitter.ColorModStartAlpha = 255;
                base.OnPropertyChanged("ColorModStartAlpha");
                particleSystem_.Emitter.ColorModStartRed = 255;
                base.OnPropertyChanged("ColorModStartRed");
                particleSystem_.Emitter.ColorModStartGreen = 0;
                base.OnPropertyChanged("ColorModStartGreen");
                particleSystem_.Emitter.ColorModStartBlue = 0;
                base.OnPropertyChanged("ColorModStartBlue");
                particleSystem_.Emitter.ColorModEndAlpha = 255;
                base.OnPropertyChanged("ColorModEndAlpha");
                particleSystem_.Emitter.ColorModeEndRed = 255;
                base.OnPropertyChanged("ColorModEndRed");
                particleSystem_.Emitter.ColorModeEndGreen = 155;
                base.OnPropertyChanged("ColorModEndGreen");
                particleSystem_.Emitter.ColorModeEndBlue = 0;
                base.OnPropertyChanged("ColorModEndBlue");
                particleSystem_.Emitter.MaxParticles = 10000;
                base.OnPropertyChanged("MaxParticles");
                particleSystem_.Emitter.Frequency = 0.0;
                base.OnPropertyChanged("Frequency");
                particleSystem_.Emitter.SprayAngle = 60;
                base.OnPropertyChanged("Angle");
                particleSystem_.Emitter.MinLife = 0.2;
                base.OnPropertyChanged("MinLife");
                particleSystem_.Emitter.MaxLife = 1.3;
                base.OnPropertyChanged("MaxLife");
                particleSystem_.Emitter.Duration = 1;
                //base.OnPropertyChanged("Duration");
                particleSystem_.Emitter.PreserveRatio = true;
                base.OnPropertyChanged("PreserveRatio");
                particleSystem_.Emitter.Particles.Clear();
                particleSystem_.Emitter.ScaleModStart = 0.3;
                base.OnPropertyChanged("ScaleModStart");
                particleSystem_.Emitter.ScaleModEnd = 3.0;
                base.OnPropertyChanged("ScaleModEnd");
                particleSystem_.Emitter.Orientation = 90;
                base.OnPropertyChanged("Orientation");
            }
            else if(preset == ParticlePreset.Fireworks)
            {
                particleSystem_.Emitter.MinOpacity = 0.1;
                base.OnPropertyChanged("MinOpacity");
                particleSystem_.Emitter.MaxOpacity = 1;
                base.OnPropertyChanged("MaxOpacity");
                particleSystem_.Emitter.MinSpeed = 100;
                base.OnPropertyChanged("MinSpeed");
                particleSystem_.Emitter.MaxSpeed = 256;
                base.OnPropertyChanged("MaxSpeed");
                particleSystem_.Emitter.MinQuantity = 450;
                base.OnPropertyChanged("MinQuantity");
                particleSystem_.Emitter.MaxQuantity = 550;
                base.OnPropertyChanged("MaxQuantity");
                particleSystem_.Emitter.Diffusion = DiffusionType.Point;
                base.OnPropertyChanged("DiffusionType");
                particleSystem_.Emitter.MinScaleX = 0.1;
                base.OnPropertyChanged("MinScaleX");
                particleSystem_.Emitter.MinScaleY = 0.1;
                base.OnPropertyChanged("MinScaleY");
                particleSystem_.Emitter.MaxScaleX = 0.2;
                base.OnPropertyChanged("MaxScaleX");
                particleSystem_.Emitter.MaxScaleY = 0.2;
                base.OnPropertyChanged("MaxScaleY");
                particleSystem_.Emitter.ColorModStartAlpha = 255;
                base.OnPropertyChanged("ColorModStartAlpha");
                particleSystem_.Emitter.ColorModStartRed = 255;
                base.OnPropertyChanged("ColorModStartRed");
                particleSystem_.Emitter.ColorModStartGreen = 255;
                base.OnPropertyChanged("ColorModStartGreen");
                particleSystem_.Emitter.ColorModStartBlue = 255;
                base.OnPropertyChanged("ColorModStartBlue");
                particleSystem_.Emitter.ColorModEndAlpha = 255;
                base.OnPropertyChanged("ColorModEndAlpha");
                particleSystem_.Emitter.ColorModeEndRed = 255;
                base.OnPropertyChanged("ColorModEndRed");
                particleSystem_.Emitter.ColorModeEndGreen = 255;
                base.OnPropertyChanged("ColorModEndGreen");
                particleSystem_.Emitter.ColorModeEndBlue =255;
                base.OnPropertyChanged("ColorModEndBlue");
                particleSystem_.Emitter.MaxParticles = 10000;
                base.OnPropertyChanged("MaxParticles");
                particleSystem_.Emitter.Frequency = 1.0;
                base.OnPropertyChanged("Frequency");
                particleSystem_.Emitter.SprayAngle = 360;
                base.OnPropertyChanged("Angle");
                particleSystem_.Emitter.MinLife = 0.2;
                base.OnPropertyChanged("MinLife");
                particleSystem_.Emitter.MaxLife = 1.0;
                base.OnPropertyChanged("MaxLife");
                particleSystem_.Emitter.Duration = 1;
                //base.OnPropertyChanged("Duration");
                particleSystem_.Emitter.PreserveRatio = true;
                base.OnPropertyChanged("PreserveRatio");
                particleSystem_.Emitter.Particles.Clear();
                particleSystem_.Emitter.ScaleModStart = 0.2;
                base.OnPropertyChanged("ScaleModStart");
                particleSystem_.Emitter.ScaleModEnd = 0.33;
                base.OnPropertyChanged("ScaleModEnd");
                particleSystem_.Emitter.Orientation = 0;
                base.OnPropertyChanged("Orientation");
            }
            else
            {
                particleSystem_.Emitter.MinOpacity = 0.1;
                base.OnPropertyChanged("MinOpacity");
                particleSystem_.Emitter.MaxOpacity = 1;
                base.OnPropertyChanged("MaxOpacity");
                particleSystem_.Emitter.MinSpeed = 40;
                base.OnPropertyChanged("MinSpeed");
                particleSystem_.Emitter.MaxSpeed = 250;
                base.OnPropertyChanged("MaxSpeed");
                particleSystem_.Emitter.MinQuantity = 30;
                base.OnPropertyChanged("MinQuantity");
                particleSystem_.Emitter.MaxQuantity = 45;
                base.OnPropertyChanged("MaxQuantity");
                particleSystem_.Emitter.Diffusion = DiffusionType.Point;
                base.OnPropertyChanged("DiffusionType");
                particleSystem_.Emitter.MinScaleX = 0.3;
                base.OnPropertyChanged("MinScaleX");
                particleSystem_.Emitter.MinScaleY = 0.3;
                base.OnPropertyChanged("MinScaleY");
                particleSystem_.Emitter.MaxScaleX = 1;
                base.OnPropertyChanged("MaxScaleX");
                particleSystem_.Emitter.MaxScaleY = 1;
                base.OnPropertyChanged("MaxScaleY");
                particleSystem_.Emitter.ColorModStartAlpha = 255;
                base.OnPropertyChanged("ColorModStartAlpha");
                particleSystem_.Emitter.ColorModStartRed = 255;
                base.OnPropertyChanged("ColorModStartRed");
                particleSystem_.Emitter.ColorModStartGreen = 255;
                base.OnPropertyChanged("ColorModStartGreen");
                particleSystem_.Emitter.ColorModStartBlue = 255;
                base.OnPropertyChanged("ColorModStartBlue");
                particleSystem_.Emitter.ColorModEndAlpha = 255;
                base.OnPropertyChanged("ColorModEndAlpha");
                particleSystem_.Emitter.ColorModeEndRed = 255;
                base.OnPropertyChanged("ColorModEndRed");
                particleSystem_.Emitter.ColorModeEndGreen = 255;
                base.OnPropertyChanged("ColorModEndGreen");
                particleSystem_.Emitter.ColorModeEndBlue = 255;
                base.OnPropertyChanged("ColorModEndBlue");
                particleSystem_.Emitter.MaxParticles = 10000;
                base.OnPropertyChanged("MaxParticles");
                particleSystem_.Emitter.Frequency = 1.5;
                base.OnPropertyChanged("Frequency");
                particleSystem_.Emitter.SprayAngle = 0;
                base.OnPropertyChanged("Angle");
                particleSystem_.Emitter.MinLife = 0.5;
                base.OnPropertyChanged("MinLife");
                particleSystem_.Emitter.MaxLife = 1.5;
                base.OnPropertyChanged("MaxLife");
                particleSystem_.Emitter.Duration = 1;
                //base.OnPropertyChanged("Duration");
                particleSystem_.Emitter.PreserveRatio = true;
                base.OnPropertyChanged("PreserveRatio");
                particleSystem_.Emitter.Particles.Clear();
                particleSystem_.Emitter.ScaleModStart = 1;
                base.OnPropertyChanged("ScaleModStart");
                particleSystem_.Emitter.ScaleModEnd = 1;
                base.OnPropertyChanged("ScaleModEnd");
                particleSystem_.Emitter.Orientation = 90;
                base.OnPropertyChanged("Orientation");
                particleSystem_.Emitter.MinSpinSpeed = 45;
                base.OnPropertyChanged("MinSpinSpeed");
                particleSystem_.Emitter.MaxSpinSpeed = 45;
                base.OnPropertyChanged("MaxSpinSpeed");
            }
        }

        #endregion //Properties

    }
}
