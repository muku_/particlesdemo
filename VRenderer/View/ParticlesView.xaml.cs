﻿//
// XNA/WPF Particles Demo 
//
// original code : Vladimiros Papaditsas
//
// (c) 2014, All rights reserved.
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GuerillaDemo.View
{
    /// <summary>
    /// Interaction logic for ParticlesView.xaml
    /// </summary>
    public partial class ParticlesView : UserControl
    {
        public ParticlesView()
        {
            InitializeComponent();
        }
    }
}
